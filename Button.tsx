import { StyleSheet, TouchableOpacity } from 'react-native';
import { Color } from '@/config/theme-color';

/* Packages */

/* Navigation & Components */
// import Loading from '@/components/Elements/Loading';
import LoadingDots from "@apolloeagle/loading-dots";
import Text from '@/components/Elements/Text';

/* Utils & Types */
import { actualSize } from '@/utils/helper';
import { ButtonCore } from '@/types/global';
import { ExitIcon, ReloadIcon } from 'assets/images/icons';


const CoreButton = ({
	button_type = '',
	button_color = '',
	button_style = {},
	title_style = {},
	title = '',
	onPress = () => { },
	loading = false,
	disabled = false,
	opacityActive = 0.5,
	children,
	IconRight,
	IconLeft,
}: ButtonCore) => {
	return (
		<>
			<TouchableOpacity activeOpacity={opacityActive} disabled={disabled || loading} onPress={() => onPress()}
				style={[styles.default, button_style, disabled && styles.disabled]}>
					{IconLeft && <IconLeft />}
				{loading ? (
					<LoadingDots animation={"flashing"} color={"#fff"} />
				) : title ? (
					<Text
						style={{
							...styles.title_default,
							...title_style,
							...(disabled && styles.disabled_title),
						}}>
						{title}
					</Text>
				) : (
					[children]
				)}
				{IconRight && <IconRight />}
			</TouchableOpacity>
		</>
	);
};

export default function Button({
	button_type = '',
	button_color = '',
	button_style = {},
	title_style = {},
	title = '',
	onPress = () => { },
	loading = false,
	disabled = false,
	opacityActive = 0.5,
	primary = true,
	children,
	IconRight,
	IconLeft
}: any) {
	const props = {
		button_type,
		button_color,
		button_style,
		title_style,
		title,
		onPress,
		loading,
		disabled,
		primary,
		children,
		IconRight,
		IconLeft
	};
	return <CoreButton {...props} />;
}

const styles = StyleSheet.create({
	default: {
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'row',
		padding: actualSize(14),
		// borderRadius: actualSize(11),
		backgroundColor: Color.MAIN,
	},
	title_default: {
		textAlign: 'center',
		color: Color.WHITE,
		fontWeight: '700',
		fontSize: actualSize(15),
		marginHorizontal: actualSize(10),
	},
	disabled: {
		backgroundColor: Color.DISABLED,
	},
	disabled_title: {
		color: Color.TEXT_PLACEHOLDER,
	},

	//***** VIOLET GRADIENT BUTTON *****//


	//****** GREEN BUTTON *******//


});

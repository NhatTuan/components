import {createRef} from 'react';

export const navigationRef = createRef<any>();

export function navigate(name: string, params: any) {
  return navigationRef.current?.navigate(name, params);
}

export function goBack() {
  return navigationRef.current?.goBack();
}

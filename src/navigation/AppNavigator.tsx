import React from 'react';
import HomeScreen from 'pages/HomeScreen/index';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {navigationName} from 'navigation/navigationName';

const HomeStack = createStackNavigator();
const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator
      initialRouteName={navigationName.homeScreen}
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.SlideFromRightIOS,
      }}>
      <HomeStack.Screen name={navigationName.homeScreen} component={HomeScreen} />
    </HomeStack.Navigator>
  );
};

const MainStack = createStackNavigator();
const MainStackScreen = () => {
  return (
    <MainStack.Navigator
      screenOptions={{
        headerShown: false,
        ...TransitionPresets.SlideFromRightIOS,
      }}>
      <MainStack.Screen name={'home'} component={HomeStackScreen} />
    </MainStack.Navigator>
  );
};

/** App navigator */
export default function AppNavigator() {
  return <MainStackScreen />;
}

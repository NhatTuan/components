import { Dimensions, Platform } from "react-native";

/** screen size */
export const WIDTH_DESIGN = 375;
export const HEIGHT_DESIGN = 812;
export const WIDTH_WINDOW = Dimensions.get('window').width;
export const HEIGHT_WINDOW = Dimensions.get('window').height;

/** Platform */
export const IS_IOS = Platform.OS === 'ios';
export const IS_ANDROID = Platform.OS === 'android';
import { WIDTH_DESIGN, WIDTH_WINDOW } from "./constant";

export const actualSize = (sizeDesign: number) => {
    return (WIDTH_WINDOW / WIDTH_DESIGN) * sizeDesign;
};

export const formatDate = (date: Date, format: string) => {
    const map: { [key: string]: string | number } = {
        'dd': ('0' + date.getDate()).slice(-2),
        'mm': ('0' + (date.getMonth() + 1)).slice(-2),
        'yyyy': date.getFullYear(),
        'yy': ('' + date.getFullYear()).slice(-2),
        'HH': ('0' + date.getHours()).slice(-2),
        'MM': ('0' + date.getMinutes()).slice(-2),
        'SS': ('0' + date.getSeconds()).slice(-2)
    };

    return format.replace(/dd|mm|yyyy|yy|HH|MM|SS/gi, matched => map[matched as keyof typeof map] as string);
}

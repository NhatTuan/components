import React from 'react';
import { StyleSheet, View } from 'react-native';
import Text from './Text';
import { actualSize } from '@/utils/helper';
import { Color } from '@/config/theme-color';

export default function ErrorText({ text, styleContainer, styleText }: any): React.JSX.Element {
    return (
        <View style={[styles.container, styleContainer]}>
            <Text style={{ ...styles.text, ...styleText }}>{text}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        // width: actualSize(295),
        // paddingTop: actualSize(5),
    },
    text: {
        fontSize: actualSize(12),
        fontFamily: 'Montserrat',
        fontWeight: '700',
        color: Color.TEXT_ERROR,
    },
});
